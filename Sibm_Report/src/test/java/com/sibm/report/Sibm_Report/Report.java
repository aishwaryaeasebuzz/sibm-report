package com.sibm.report.Sibm_Report;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import junit.framework.Assert;

public class Report {

	final static Logger log = Logger.getLogger(Report.class);

	WebDriver driver = null;
	String url = null;
	public static By prnno = By.xpath("//div[@class='m-content pt-4']/div/div/div/div/div/div[2]/div[2]/div/input");
	public static By selectCourse = By.xpath("//div[@class='m-content pt-4']/div/div/div/div/div/div[1]/div/select");
	public static By selectBatch = By.xpath("//div[@class='m-content pt-4']/div/div/div/div/div/div[1]/div[2]/select");
	public static By selectsemName = By
			.xpath("//div[@class='m-content pt-4']/div/div/div/div/div/div[1]/div[3]/select");
	public static By selectSpecialization = By
			.xpath("//div[@class='m-content pt-4']/div/div/div/div/div/div[1]/div[4]/select");

	public static By listSub = By.xpath("//div[@class='m-widget2__desc']/span[1]");
	public static By perSub = By.xpath("//div[@class='m-widget25--progress']/div/div[2]");
	public static By sbujectName = By.xpath("//div[@id='m_widget2_tab1_content']/div/div[2]/div[2]/span[1]");
	public static By percentage = By
			.xpath("//div[@id='m_widget2_tab1_content']/div/div[2] /div[3]/div/div[2]/div/div/div/span");

	public Report(WebDriver driver) {

		this.driver = driver;

	}

	public void clickanalytics() throws InterruptedException {
		driver.findElement(By.xpath("//a[@href='/analytics/dashboard']")).click();

		url = driver.getCurrentUrl();

		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

		
		System.out.println("Analytics  URL:-" + url);

		log.info("Analytics  URL:-" + url);

		Thread.sleep(1000);

		driver.findElement(By.xpath("//a[@href='/analytics/overall']")).click();

		url = driver.getCurrentUrl();
		
 
		System.out.println("AalyticsOverall  URL:-" + url);
		log.info("AalyticsOverall  URL:-" + url);
		
		
		
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	
		
		System.out.println();
		System.out.println("**********Weclome to Subject List***************************");
		
		log.info("*********************************************************************");

		log.info("**********Weclome to Subject List***************************");
		
		log.info("*********************************************************************");

	}

	private void overAllReport(String CourseName, String batchName, String semName1, String specialization)
			throws InterruptedException {

		Thread.sleep(1000);
		WebElement sel = driver.findElement(selectCourse);
		Select se = new Select(sel);
		se.selectByVisibleText(CourseName);

		WebElement sel1 = driver.findElement(selectBatch);
		Select se1 = new Select(sel1);
		se1.selectByVisibleText(batchName);

		WebElement sel2 = driver.findElement(selectsemName);
		Select se2 = new Select(sel2);
		se2.selectByVisibleText(semName1);

		WebElement sel3 = driver.findElement(selectSpecialization);
		Select se3 = new Select(sel3);
		se3.selectByVisibleText(specialization);

	}

	public void attendanceView(String sub1, String per1, String sub2, String per2, String sub3, String per3,
			String sub4, String per4, String sub5, String per5, String sub6, String per6, String sub7, String per7,
			String sub8, String per8, String sub9, String per9, String sub10, String per10, String sub11, String per11,
			String sub12, String per12, String sub13, String per13, String sub14, String per14, String sub15,
			String per15, String sub16, String per16, String sub17, String per17, String sub18, String per18,
			String sub19, String per19) throws InterruptedException {

		Thread.sleep(1000);

		Set<String> handle = driver.getWindowHandles();
		Iterator<String> it = handle.iterator();

		String main = it.next();
		String tab = it.next();
		driver.switchTo().window(tab);

		List<WebElement> text = driver.findElements(listSub);
		List<WebElement> text1 = driver.findElements(perSub);
		Thread.sleep(1000);

		int n = text.size();

		System.out.println("size:-" + n);

		for (int i = 0; i < text.size(); i++)

		{

			Thread.sleep(4000);

		
			System.out.println("SubjectNo on webPage:" + i);
			log.info("SubjectNo on webPage:" + i);

			Thread.sleep(2000);

			String data = text.get(i).getText();

			System.out.println("SubjectName on webPage:-" + data);
			log.info("SubjectName on webPage:-" + data);

			String withoutspaceonweb = data.replaceAll("\\s", "");

			System.out.println("withoutspace=" + withoutspaceonweb);

			Thread.sleep(4000);

			String data1 = text1.get(i).getText();

			System.out.println("Percentage on webPage:-" + data1);

			log.info("Percentage on webPage:-" + data1);
			Thread.sleep(4000);

			if (withoutspaceonweb.equals(sub1)) {
				System.out.println("*********Successfully match subject with web and Excelsheet =" + sub1);
				log.info("*********Successfully match subject with web and Excelsheet =" + sub1);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per1);
				log.info("Percentage on Excel Sheet that u enter:-"+per1);
                 
				
				if (data1.equals(per1)) {
					
					

					System.out.println("*********successfully  match percentage of" + data + " with web and Excel ");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println("per1=" + per1);
					System.out.println();
					System.out.println("************************************************");
					
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					log.error(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println("************************************************");
					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub2)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub2);
				log.info("*********Successfully match subject with web and Excelsheet =" + sub2);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per2);
				log.info("Percentage on Excel Sheet that u enter:-"+per2);

				if (data1.equals(per2)) {
					
					

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					log.error(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println("************************************************");
					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub3)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub3);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub3);
				
				System.out.println("Percentage on Excel Sheet that u enter:-"+per3);
				log.info("Percentage on Excel Sheet that u enter:-"+per3);

				if (data1.equals(per3)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					log.error(data + " percentage not match with Webpage and Excel Sheet");
					
					System.out.println("************************************************");
					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub4)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub4);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub4);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per4);
				log.info("Percentage on Excel Sheet that u enter:-"+per4);

				if (data1.equals(per4)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println("************************************************");
					log.error(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub5)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub5);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub5);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per5);
				log.info("Percentage on Excel Sheet that u enter:-"+per5);
				if (data1.equals(per5)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println("************************************************");
					log.error(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub6)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub6);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub6);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per6);
				log.info("Percentage on Excel Sheet that u enter:-"+per6);

				if (data1.equals(per6)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println("************************************************");
					log.error(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub7)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub7);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub7);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per7);
				log.info("Percentage on Excel Sheet that u enter:-"+per7);

				if (data1.equals(per7)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println("************************************************");
					log.error(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub8)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub8);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub8);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per8);
				log.info("Percentage on Excel Sheet that u enter:-"+per8);
				if (data1.equals(per8)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println("************************************************");
					log.error(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub9)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub9);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub9);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per9);
				log.info("Percentage on Excel Sheet that u enter:-"+per9);

				if (data1.equals(per9)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println("************************************************");
					log.error(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub10)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub10);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub10);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per10);
				log.info("Percentage on Excel Sheet that u enter:-"+per10);

				if (data1.equals(per10)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println("************************************************");
					log.error(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub11)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub11);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub11);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per11);
				log.info("Percentage on Excel Sheet that u enter:-"+per11);

				if (data1.equals(per11)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println("************************************************");
					log.error(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub12)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub12);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub12);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per12);
				log.info("Percentage on Excel Sheet that u enter:-"+per12);

				if (data1.equals(per12)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println("************************************************");
					log.error(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub13)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub13);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub13);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per13);
				log.info("Percentage on Excel Sheet that u enter:-"+per13);

				if (data1.equals(per13)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println("************************************************");
					log.error(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub14)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub14);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub14);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per14);
				log.info("Percentage on Excel Sheet that u enter:-"+per14);

				if (data1.equals(per14)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println("************************************************");
					log.error(data + " percentage not match with Webpage and Excel Sheet");
					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub15)) {
				System.out.println("**********Successfully match Subject with web and Excelsheet =" + sub15 + "");
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub15);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per15);
				log.info("Percentage on Excel Sheet that u enter:-"+per15);

				if (data1.equals(per15)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					log.error(data + " percentage not match with Webpage and Excel Sheet");

					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub16)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub16);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub16);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per16);
				log.info("Percentage on Excel Sheet that u enter:-"+per16);


				if (data1.equals(per16)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");


				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					log.error(data + " percentage not match with Webpage and Excel Sheet");

					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub17)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub17);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub17);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per17);
				log.info("Percentage on Excel Sheet that u enter:-"+per17);


				if (data1.equals(per17)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					log.error(data + " percentage not match with Webpage and Excel Sheet");

					System.out.println();
				}

			}

			else if (withoutspaceonweb.equals(sub18)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub18);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub18);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per18);
				log.info("Percentage on Excel Sheet that u enter:-"+per18);

				if (data1.equals(per18)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					log.error(data + " percentage not match with Webpage and Excel Sheet");

					System.out.println();
				}

			}

			
			
			else if (withoutspaceonweb.equals(sub19)) {
				System.out.println("*********Successfully match Subject with web and Excelsheet =" + sub19);
				log.info("*********Successfully match Subject with web and Excelsheet =" + sub19);
				System.out.println("Percentage on Excel Sheet that u enter:-"+per19);
				log.info("Percentage on Excel Sheet that u enter:-"+per19);

				if (data1.equals(per19)) {

					System.out.println("*********successfully  match percentage of " + data + " with web and Excel");
					log.info("*********successfully  match percentage of" + data + " with web and Excel ");

				} else {
					System.out.println();
					System.out.println("************************************************");
					System.out.println(data + " percentage not match with Webpage and Excel Sheet");
					log.error(data + " percentage not match with Webpage and Excel Sheet");

					System.out.println();
				}

			}
			else {

				System.out.println();
				System.out.println("************************************************");
				System.out.println("Search subject not found on web =" + data);
				log.error("Search subject not found on web =" + data);
				System.out.println("*************************************************");
				System.out.println();
			}
		}
	}

	/*
	 * String data1=text1.get(i).getText();
	 * 
	 * System.out.println("Percentage:-"+data1); Thread.sleep(4000);
	 */

	public void attendanceReport(String CourseName, String batchName, String semName1, String specialization,
			String prn, String sub1, String per1, String sub2, String per2, String sub3, String per3, String sub4,
			String per4, String sub5, String per5, String sub6, String per6, String sub7, String per7, String sub8,
			String per8, String sub9, String per9, String sub10, String per10, String sub11, String per11, String sub12,
			String per12, String sub13, String per13, String sub14, String per14, String sub15, String per15,
			String sub16, String per16, String sub17, String per17, String sub18, String per18, String sub19,
			String per19) throws InterruptedException {

		this.clickanalytics();
		Thread.sleep(1000);

		this.overAllReport(CourseName, batchName, semName1, specialization);

		driver.findElement(prnno).sendKeys(prn);
		System.out.println();
		System.out.println("******************************************");
		log.info("********************************************************");
		System.out.println("*****************Student PRN No:- "+prn);
		log.info("*****************Student PRN No:- "+prn);
		log.info("****************************************************");
		System.out.println("******************************************");
		System.out.println();

		Thread.sleep(1000);

		driver.findElement(By.xpath("//div[@class='table-responsive']/table/tbody/tr/td[5]/a[4]")).click();

		// /manageAttendance/479
		Thread.sleep(2000);

		this.attendanceView(sub1, per1, sub2, per2, sub3, per3, sub4, per4, sub5, per5, sub6, per6, sub7, per7, sub8,
				per8, sub9, per9, sub10, per10, sub11, per11, sub12, per12, sub13, per13, sub14, per14, sub15, per15,
				sub16, per16, sub17, per17, sub18, per18, sub19, per19);

	}

}
