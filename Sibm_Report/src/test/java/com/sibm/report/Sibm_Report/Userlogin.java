package com.sibm.report.Sibm_Report;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.sikuli.script.FindFailed;
import org.testng.Assert;





public class Userlogin {
	WebDriver driver=null;
	public static By email =By.id("username");
	public static By pwd=By.id("password");
	public static By submit=By.id("login-btn");
	int n=3876;
	final static Logger log = Logger.getLogger(Userlogin.class);
	
	public Userlogin(WebDriver driver) {
	
		this.driver=driver;
	
	}
	
	
	
	private void emailid(String emailId) {
		
		
		driver.findElement(email).sendKeys(emailId);
		
	}


	
	private void password(String password) {
		
		driver.findElement(pwd).sendKeys(password);
	}
	
	

    private void clickOnSubmit() throws InterruptedException
    
   {
	driver.findElement(submit).click();
	Thread.sleep(2000);
	
	String url=driver.getCurrentUrl();
	
	System.out.println("URL:" +url);
	
	Assert.assertTrue(url.contains("/adminDashboard"));
	log.info("Login Successful");

	
}
   

public void login(String emailId, String password) throws InterruptedException, FindFailed {
  
		
		this.emailid(emailId);
		this.password(password);
		Thread.sleep(1000);
		
		this.clickOnSubmit();
		
	
		
		
		
		
	
		

		
	}





}


	