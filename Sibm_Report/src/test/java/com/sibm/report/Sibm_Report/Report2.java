package com.sibm.report.Sibm_Report;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Report2 {

	WebDriver driver = null;
	String url = null;
	public static By prnno = By.xpath("//div[@class='m-content pt-4']/div/div/div/div/div/div[2]/div[2]/div/input");
	public static By selectCourse = By.xpath("//div[@class='m-content pt-4']/div/div/div/div/div/div[1]/div/select");
	public static By selectBatch = By.xpath("//div[@class='m-content pt-4']/div/div/div/div/div/div[1]/div[2]/select");
	public static By selectsemName = By
			.xpath("//div[@class='m-content pt-4']/div/div/div/div/div/div[1]/div[3]/select");
	public static By selectSpecialization = By
			.xpath("//div[@class='m-content pt-4']/div/div/div/div/div/div[1]/div[4]/select");

	public static By listSub = By.xpath("//div[@class='m-widget2__desc']/span[1]");
	public static By perSub = By.xpath("//div[@class='m-widget25--progress']/div/div[2]");
	public static By sbujectName = By.xpath("//div[@id='m_widget2_tab1_content']/div/div[2]/div[2]/span[1]");
	public static By percentage = By
			.xpath("//div[@id='m_widget2_tab1_content']/div/div[2] /div[3]/div/div[2]/div/div/div/span");

	public Report2(WebDriver driver) {

		this.driver = driver;

	}

	public void clickanalytics() throws InterruptedException {
		driver.findElement(By.xpath("//a[@href='/analytics/dashboard']")).click();

		url = driver.getCurrentUrl();

		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

		System.out.println("Analytics  URL:-" + url);

		Thread.sleep(1000);

		driver.findElement(By.xpath("//a[@href='/analytics/overall']")).click();

		url = driver.getCurrentUrl();

		System.out.println("AalyticsOverall  URL:-" + url);
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

		System.out.println();
		System.out.println("**********Weclome to Subject List***************************");

	}

	private void overAllReport(String CourseName, String batchName, String semName1, String specialization)
			throws InterruptedException {

		Thread.sleep(1000);
		WebElement sel = driver.findElement(selectCourse);
		Select se = new Select(sel);
		se.selectByVisibleText(CourseName);

		WebElement sel1 = driver.findElement(selectBatch);
		Select se1 = new Select(sel1);
		se1.selectByVisibleText(batchName);

		WebElement sel2 = driver.findElement(selectsemName);
		Select se2 = new Select(sel2);
		se2.selectByVisibleText(semName1);

		WebElement sel3 = driver.findElement(selectSpecialization);
		Select se3 = new Select(sel3);
		se3.selectByVisibleText(specialization);

	}

	public void attendanceView(String sub1, String per1, String sub2, String per2, String sub3, String per3,
			String sub4, String per4, String sub5, String per5, String sub6, String per6, String sub7, String per7,
			String sub8, String per8, String sub9, String per9, String sub10, String per10, String sub11, String per11,
			String sub12, String per12, String sub13, String per13, String sub14, String per14, String sub15,
			String per15, String sub16, String per16) throws InterruptedException {

		Thread.sleep(1000);

		Set<String> handle = driver.getWindowHandles();
		Iterator<String> it = handle.iterator();

		String main = it.next();
		String tab = it.next();
		driver.switchTo().window(tab);

		List<WebElement> text = driver.findElements(listSub);
		List<WebElement> text1 = driver.findElements(perSub);
		Thread.sleep(1000);

		int n = text.size();

		System.out.println("size:-" + n);

		for (int i = 0; i < text.size(); i++)

		{

			Thread.sleep(4000);

			System.out.println();
			System.out.println("SubjectNo on webPage:" + i);

			Thread.sleep(2000);

			String data = text.get(i).getText();

			System.out.println("SubjectName on webPage:-" + data);

			String withoutspaceonweb = data.replaceAll("\\s", "");

			// System.out.println("withoutspace="+withoutspace);

			Thread.sleep(4000);

			String data1 = text1.get(i).getText();

			System.out.println("Percentage on webPage:-" + data1);
			Thread.sleep(4000);

			switch(withoutspaceonweb){    
			
			    
			default:     
			    
			 System.out.println();
				System.out.println("************************************************");
				System.out.println("Search subject not found in Excel Sheet=" + data);
				System.out.println("*************************************************");
				System.out.println();
		}	
		}
}


	/*
	 * String data1=text1.get(i).getText();
	 * 
	 * System.out.println("Percentage:-"+data1); Thread.sleep(4000);
	 */

	public void attendanceReport(String CourseName, String batchName, String semName1, String specialization,
			String prn, String sub1, String per1, String sub2, String per2, String sub3, String per3, String sub4,
			String per4, String sub5, String per5, String sub6, String per6, String sub7, String per7, String sub8,
			String per8, String sub9, String per9, String sub10, String per10, String sub11, String per11, String sub12,
			String per12, String sub13, String per13, String sub14, String per14, String sub15, String per15,
			String sub16, String per16) throws InterruptedException {

		this.clickanalytics();
		Thread.sleep(1000);

		this.overAllReport(CourseName, batchName, semName1, specialization);

		driver.findElement(prnno).sendKeys(prn);
		Thread.sleep(1000);

		driver.findElement(By.xpath("//a[@href='/manageAttendance/477']")).click();

		Thread.sleep(2000);

		this.attendanceView(sub1, per1, sub2, per2, sub3, per3, sub4, per4, sub5, per5, sub6, per6, sub7, per7, sub8,
				per8, sub9, per9, sub10, per10, sub11, per11, sub12, per12, sub13, per13, sub14, per14, sub15, per15,
				sub16, per16);

		
		
		
		
		
		
	}
	
	
	
	
	

}
