package com.sibm.report.Sibm_Report;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;




public class Utility {
	

	final static Logger log = Logger.getLogger(Report.class);
	
	Userlogin ul=null;
	
	Users_SetupCourse sc=null;
	WebDriver driver=null;
	
	public static By clickOnDashboard=By.xpath("//ul[@class='m-menu__nav  m-menu__nav--dropdown-submenu-arrow']/li/a");
	
	@BeforeMethod
	public void setup() {

		System.setProperty("webdriver.chrome.driver", "D:\\EasebuzzPvtLtd\\jar_files\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
	
		
		driver.get("http://52.60.135.26:8001/");
		
		 PropertyConfigurator.configure("log4j.properties");
		driver.manage().window().maximize();
		
	
	
	}
	@Test(dataProvider="getData")
	public void signup(String emailId,String password,String CourseName,String batchName,String semName1,String specialization,String prn,String sub1,String per1,String sub2,String per2,String sub3,String per3,String sub4,String per4,
			String sub5,String per5,String sub6,String per6,String sub7,String per7,String sub8,String per8,String sub9,String per9,String sub10,String per10,String sub11,String per11,String sub12,String per12,String sub13,String per13,String sub14,String per14,String sub15,String per15,String sub16,String per16
			,String sub17, String per17, String sub18, String per18,String sub19, String per19) throws InterruptedException, FindFailed
	{
		
		ul=new  Userlogin(driver);
		 ul.login(emailId,password);
		 
	  /*sc=new Users_SetupCourse(driver);
	    sc.setupCourse();*/
	     
	
	  
	  Report re=new Report(driver);
	  re.attendanceReport(CourseName, batchName,semName1,specialization,prn,sub1,per1,sub2,per2,sub3,per3,sub4,per4
			  , sub5,per5,sub6,per6, sub7, per7,sub8, per8,sub9,per9, sub10, per10,sub11,per11,sub12, per12,sub13, per13,sub14,per14,sub15,per15,sub16,per16
			 , sub17, per17, sub18, per18,sub19, per19);
	  
	
	}
	
	@DataProvider(name="getData")

	public Object[][] getData() throws InterruptedException{
		
	
		ExcelfileReader xl=new ExcelfileReader("D:\\EasebuzzPvtLtd\\Excelfiles\\SLS\\Sibm.xlsx");
		int rowCount=xl.getRowCount("attendance_Report_PerWise");
		int columnCount=xl.getColumnCount("attendance_Report_PerWise");
		
		Object[][] data=new Object[rowCount-1][columnCount];
		
		for(int i=0;i<rowCount-1;i++)
		{
			int x=i+2;
			
			for(int j=0;j<columnCount;j++)
			{
				data[i][j]=xl.getCellData("attendance_Report_PerWise", j, x);
			}
			
			
			
		}
		
		return data;
		
	}
	
	@AfterMethod
	public void close1()
	{
		driver.close();
		 
	}


}



